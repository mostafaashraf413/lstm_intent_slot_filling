#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 24 12:20:22 2017

@author: mostafa
"""

import pandas as pd
import numpy as np
from label_encoder import LabelEncoder
from keras.preprocessing.sequence import pad_sequences
from sklearn.preprocessing import OneHotEncoder
import pickle
from functools import reduce

class Preprocessing:
    
    def __init__(self, to_lower_case=True, handle_digits=True):
        self.to_lower_case = to_lower_case
        self.handle_digits = handle_digits
    
    def __convert_to_lower_case(self, str_):
        if self.to_lower_case:
            return str_.lower()
        else:
            return str_
    
    def white_space_tokenizer(self, str_):
        return str_.split()
    
    def replace_digits(self, tokens):
        if self.handle_digits:
            result_tokens = []
            for token in tokens:
                new_token = token
                if token.isdigit():
                    new_token = '#digit'*len(token)
                result_tokens.append(new_token)
            return result_tokens
        else:
            return tokens
    
    def split_XY(self, file_name):
        df = pd.read_csv(filepath_or_buffer = file_name, sep = '\t', header = None, dtype=str, encoding='utf8')
        values = df.values
        return values[:,0], values[:,1]
    
    def pad(self, data_2d, maxlen=None, padding='pre'):
        # data is a 2d array
        return pad_sequences(data_2d, maxlen=maxlen, padding=padding)
    
    def reshape_to_3d(self, _2d, shape):
        _3d = np.zeros(shape = shape)
        for i in range(_3d.shape[0]):
            for j in range(_3d.shape[1]):
                _3d[i][j] = _2d[i, j*shape[2]:(j*shape[2])+shape[2]]
        return _3d

    def preprocess_training_data(self, file_name):
        X,Y = self.split_XY(file_name)
        X = list(map(self.__convert_to_lower_case, X))
        X = list(map(self.white_space_tokenizer, X))
        X = list(map(self.replace_digits, X))
        Y = list(map(self.white_space_tokenizer, Y))
        for i in range(len(X)):
            assert(len(X[i]) == len(Y[i]))
            
        # encode X and Y
        self.x_encoder = LabelEncoder(X)
        self.y_encoder = LabelEncoder(Y)
        
        X = [self.x_encoder.encode(i) for i in X]
        Y = [self.y_encoder.encode(i) for i in Y]
        
        self.maxlen = reduce(lambda a,b: a if a>b else b, map(len, X))
        X = self.pad(X, maxlen=self.maxlen, padding='pre')
        Y = self.pad(Y, maxlen=self.maxlen, padding='pre')
        
        # encoding Y (one hot encoding)
        self.one_hot_encoder = OneHotEncoder(n_values = self.y_encoder.get_label_count(), sparse=False)
        Y = [self.one_hot_encoder.fit_transform(np.array(i).reshape(1, len(i))) for i in Y]
        Y = np.array([i[0].tolist() for i in Y])
        
        # reshaping Y to 3d
        Y = self.reshape_to_3d(Y, (Y.shape[0], int(Y.shape[1]/self.y_encoder.get_label_count()), self.y_encoder.get_label_count()))
        return X,Y, self.x_encoder.get_label_count()

    def preprocess_test_data(self, file_name):
        X,Y = self.split_XY(file_name)
        X = list(map(self.__convert_to_lower_case, X))
        X = list(map(self.white_space_tokenizer, X))
        X = list(map(self.replace_digits, X))
        Y = list(map(self.white_space_tokenizer, Y))
        for i in range(len(X)):
            assert(len(X[i]) == len(Y[i]))
            
        X = [self.x_encoder.encode(i) for i in X]
        Y = [self.y_encoder.encode(i) for i in Y]
        
        X = self.pad(X, maxlen=self.maxlen, padding='pre')
        Y = self.pad(Y, maxlen=self.maxlen, padding='pre')
        
        # encoding Y (one hot encoding)
        Y = [self.one_hot_encoder.fit_transform(np.array(i).reshape(1, len(i))) for i in Y]
        Y = np.array([i[0].tolist() for i in Y])
        
        # reshaping Y to 3d
        Y = self.reshape_to_3d(Y, (Y.shape[0], int(Y.shape[1]/self.y_encoder.get_label_count()), self.y_encoder.get_label_count()))
        return X,Y
    
    def preprocess_x(self, x, add_bos_eos=True):
        # convert string to lowercase
        x = self.__convert_to_lower_case(x)
        # tokenize sentence
        x = self.white_space_tokenizer(x)
        # add sentence docerators
        if add_bos_eos:
            x = ['BOS'] + x + ['EOS']
        # replace numbers with #digit strings
        x = self.replace_digits(x)
        # label encode
        x = self.x_encoder.encode(x)
        # prepare x to be passed to keras sequence padding unction
        x = [x]
        # sequence padding
        x = self.pad(x, maxlen=self.maxlen, padding='pre')
        # get only the padded vector
#        x = x[0]
        # convert it to numpy 
#        x = np.array(x).reshape(1, len(x))
        return x
    
    def convert_to_iob_tags(self, y):
        _2d = self.__decode_classifier_output(y)
        decoded = _2d[0]
        decoded = self.y_encoder.decode(decoded)
        return decoded

    def __decode_classifier_output(self, arr3d):
#        output_vec = [i.argmax() for i in _2d_output]
#        return output_vec
        output_2d = np.zeros(shape=(arr3d.shape[0], arr3d.shape[1]), dtype=int)
        for i in range(arr3d.shape[0]):
            output_2d[i] = [j.argmax() for j in arr3d[i]]
            
        return output_2d
    
    def save(self, path):
        with open(path, 'wb') as pre_file:
            pickle.dump(self, pre_file, protocol=pickle.HIGHEST_PROTOCOL)
         
    @staticmethod
    def load(path):
        preprocessor = None
        with open(path, 'rb') as f:
            preprocessor = pickle.load(f)
        return preprocessor