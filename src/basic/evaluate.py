# -*- coding: utf-8 -*-
"""
Created on Tue Jan 16 21:16:09 2018

@author: Wahdan
"""

# imports
from preprocessing import Preprocessing
from blstm_joint_tagger import BLSTMJointTagger
import pandas as pd

# parameters
testing_file = '../../resources/basic/test/atis.test.w-intent.iob'
testing_output = '../../resources/basic/test/atis.test.w-intent.result.iob'
model_file = '../../resources/basic/model/intent_slot_filling.model'
preprocessing_file = '../../resources/basic/model/intent_slot_filling_preprocessing.pickle'

# load model
print('loading model ....')
tagger = BLSTMJointTagger.load(model_file)
preprocessor = Preprocessing.load(preprocessing_file)

# preprocessing
print('start testing ....')
df = pd.read_csv(filepath_or_buffer = testing_file, sep = '\t', header = None, dtype=str, encoding='utf8')
values = df.values
sentences = values[:,0]
tags = values[:,1]

# test data
out = ''
for sentence, tag in zip(sentences, tags):
    golds = tag.split()
    words = sentence.split()
    input_vector = preprocessor.preprocess_x(sentence, False)
    output_vector = tagger.predict(input_vector)
    tag_sequence = preprocessor.convert_to_iob_tags(output_vector)
    tag_sequence = ' '.join([element for element in tag_sequence if element != '<PAD>'])
#    tag_sequence = ' '.join(tag_sequence[len(words):])
    predictions = tag_sequence.split()
    assert len(golds) == len(words) == len(predictions)
    for i in range(len(golds)):
        out += words[i] + ' ' + golds[i] + ' ' + predictions[i] + '\n'
    out += '\n'
file = open(testing_output, mode='w', encoding="utf8")
file.writelines(out)
file.close()
