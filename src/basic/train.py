#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 25 09:50:58 2017

@author: wahdan
"""

from preprocessing import Preprocessing
from blstm_joint_tagger import BLSTMJointTagger

# parameters
training_file = '../../resources/basic/train/atis.train.w-intent.iob'
save_model_to = '../../resources/basic/model/intent_slot_filling.model'
save_preprocessing_to = '../../resources/basic/model/intent_slot_filling_preprocessing.pickle'

# preprocessing
print('start preprocessing ....')
preprocessor = Preprocessing()
X, Y, vocab_size = preprocessor.preprocess_training_data(training_file)

# train the classifier
print('start training ....')
tagger = BLSTMJointTagger()
tagger.train(X, Y, vocab_size=vocab_size)

# save model weights and preprocessing (encoders)
print('saving model  ....')
tagger.save(save_model_to)
preprocessor.save(save_preprocessing_to)
