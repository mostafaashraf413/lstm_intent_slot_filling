#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 24 14:48:35 2017

@author: mostafa
"""

from sklearn.preprocessing import OneHotEncoder
import numpy as np

class LabelEncoder:
    def __init__(self, data, handle_padding = True, handle_unknown = True, pad_label = None, unk_label = None):
        self.idx2label = []
        self.label2idx = {}
        self.label_code_counter = 0
        self.pad_label = pad_label
        self.unk_label = unk_label
        
        if handle_padding:
            self.idx2label.append(self.pad_label)
            self.label2idx[self.pad_label] = self.label_code_counter
            self.label_code_counter+=1
            
        if handle_unknown:
            self.idx2label.append(self.unk_label)
            self.label2idx[self.unk_label] = self.label_code_counter
            self.label_code_counter+=1
        
        for line in data:
            for label in line:
                if label not in self.label2idx:
                    self.idx2label.append(label)
                    self.label2idx[label] = self.label_code_counter
                    self.label_code_counter+=1
            
    def encode(self, seq):
        def encode_label(label):
            if label in self.label2idx:
                return self.label2idx[label]
            return self.label2idx[self.unk_label]
            
        encoded_seq = [encode_label(i) for i in seq]
        return encoded_seq
    
    def decode(self, seq):
        decoded_seq = [self.idx2label[i] for i in seq]
        return decoded_seq
    
    def is_label_exist(self, label):
        return label in self.label2idx
            
    def get_label_count(self):
        return len(self.idx2label)
    