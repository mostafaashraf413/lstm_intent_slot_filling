#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 24 12:20:22 2017

@author: mostafa
"""

import pandas as pd
import numpy as np
from label_encoder import LabelEncoder
from keras.preprocessing.sequence import pad_sequences
from sklearn.preprocessing import OneHotEncoder
import pickle
from functools import reduce


class Preprocessing:
    
    def __init__(self, to_lower_case=True, handle_digits=True, embeddings_file=None, embedding_dim = 300):
        self.to_lower_case = to_lower_case
        self.handle_digits = handle_digits
        self.embedding_dim = embedding_dim
        self.__create_word_embedding_dic(embeddings_file, embedding_dim)
        self.pad_label = '<PAD>'
        self.unk_label = '<UNK>'
        self.digit_label = '#digits'
        self.bos_label = 'BOS'
        self.eos_label = 'EOS'
        
    # url: https://blog.keras.io/using-pre-trained-word-embeddings-in-a-keras-model.html
    # tested only on glove word embeddings
    def __create_word_embedding_dic(self, embeddings_file, embedding_dim):
        self.embeddings_index = {}
        f = open(embeddings_file, encoding="utf8")
        print('reading embeddings from %s'%(embeddings_file))
        for line in f:
            values = line.split()
            
            if len(values) != embedding_dim+1:
                print('WARNING: bad embedding format [%s ....]'%(' '.join(values[:20])))
                continue
            
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            self.embeddings_index[word] = coefs
           
        f.close()
        print('Found %s word vectors.' % len(self.embeddings_index))
        
    def __get_word_embedding(self, word):
        if word in self.embeddings_index:
            return self.embeddings_index[word]
        
        word = word[0].upper()+word[0:]
        if word in self.embeddings_index:
            return self.embeddings_index[word]
        
        #if the word doesn't exist in word embedding dictionary return vector of Zero's
        return np.zeros(self.embedding_dim)
    
    def __convert_to_lower_case(self, str_):
        if self.to_lower_case:
            return str_.lower()
        else:
            return str_
    
    def white_space_tokenizer(self, str_):
        return str_.split()
    
    def replace_digits(self, tokens):
        if self.handle_digits:
            result_tokens = []
            for token in tokens:
                new_token = token
                if token.isdigit():
                    #replace whole number with one #digits code
                    new_token = self.digit_label#*len(token)
                result_tokens.append(new_token)
            return result_tokens
        else:
            return tokens
    
    def split_XY(self, file_name):
        df = pd.read_csv(filepath_or_buffer = file_name, sep = '\t', header = None, dtype=str, encoding='utf8')
        values = df.values
        return values[:,0], values[:,1]
    
    def pad(self, data_2d, maxlen=None, padding='pre', value=0, dtype='int32'):
        # data is a 2d array
        return pad_sequences(data_2d, maxlen=maxlen, padding=padding, value=value, dtype=dtype)
    
    def reshape_to_3d(self, _2d, shape):
        _3d = np.zeros(shape = shape)
        for i in range(_3d.shape[0]):
            for j in range(_3d.shape[1]):
                _3d[i][j] = _2d[i, j*shape[2]:(j*shape[2])+shape[2]]
        return _3d
    
    #input: utterance (array of strings)
    #output: encoded utterance (array of strings)
    def encode_x_utterance(self, utterance):
        encoded_utterance = []
        
        for word in utterance:
            
            if word == '0':
                word = self.pad_label
            
            encoded_word = list(self.__get_word_embedding(word))
            
            #is digit
            encoded_word += [1 if word == self.digit_label else 0]
            #is BOS
            encoded_word += [1 if word == self.bos_label else 0]
            #is EOS
            encoded_word += [1 if word == self.eos_label else 0]
            #is UNK
            #encoded_atterance += 1 if word ==
            #is PAD
            encoded_word += [1 if word == self.pad_label else 0]
            
            encoded_utterance.append(encoded_word)
            
        return encoded_utterance

    def preprocess_training_data(self, file_name):
            
        X,Y = self.split_XY(file_name)
        X = list(map(self.__convert_to_lower_case, X))
        X = list(map(self.white_space_tokenizer, X))
        X = list(map(self.replace_digits, X))
        Y = list(map(self.white_space_tokenizer, Y))
        for i in range(len(X)):
            assert(len(X[i]) == len(Y[i]))
            
        # encode X
        self.maxlen = reduce(lambda a,b: a if a>b else b, map(len, X))
        X = self.pad(X, maxlen=self.maxlen, padding='pre', dtype=str)
        X = [self.encode_x_utterance(i) for i in X]
        
        
        # encode Y
        self.y_encoder = LabelEncoder(Y, pad_label = self.pad_label, unk_label = self.unk_label) 
        Y = [self.y_encoder.encode(i) for i in Y]
        self.maxlen = reduce(lambda a,b: a if a>b else b, map(len, Y))
        Y = self.pad(Y, maxlen=self.maxlen, padding='pre')
        
        # encoding Y (one hot encoding)
        self.one_hot_encoder = OneHotEncoder(n_values = self.y_encoder.get_label_count(), sparse=False)
        Y = [self.one_hot_encoder.fit_transform(np.array(i).reshape(1, len(i))) for i in Y]
        Y = np.array([i[0].tolist() for i in Y])
        
        # reshaping Y to 3d
        Y = self.reshape_to_3d(Y, (Y.shape[0], int(Y.shape[1]/self.y_encoder.get_label_count()), self.y_encoder.get_label_count()))
        return np.array(X), Y #, self.x_encoder.get_label_count()

    def preprocess_test_data(self, file_name):
        X,Y = self.split_XY(file_name)
        X = list(map(self.__convert_to_lower_case, X))
        X = list(map(self.white_space_tokenizer, X))
        X = list(map(self.replace_digits, X))
        Y = list(map(self.white_space_tokenizer, Y))
        for i in range(len(X)):
            assert(len(X[i]) == len(Y[i]))
            
        # encode X
        X = self.pad(X, maxlen=self.maxlen, padding='pre', dtype=str)
        X = [self.encode_x_utterance(i) for i in X]
        
        #encode Y
        Y = [self.y_encoder.encode(i) for i in Y]
        Y = self.pad(Y, maxlen=self.maxlen, padding='pre')
        
        # encoding Y (one hot encoding)
        Y = [self.one_hot_encoder.fit_transform(np.array(i).reshape(1, len(i))) for i in Y]
        Y = np.array([i[0].tolist() for i in Y])
        
        # reshaping Y to 3d
        Y = self.reshape_to_3d(Y, (Y.shape[0], int(Y.shape[1]/self.y_encoder.get_label_count()), self.y_encoder.get_label_count()))
        return np.array(X),Y
    
    def preprocess_x(self, x):
        # convert string to lowercase
        x = self.__convert_to_lower_case(x)
        # tokenize sentence
        x = self.white_space_tokenizer(x)
        # add sentence docerators
        x = ['BOS'] + x + ['EOS']
        # replace numbers with #digit strings
        x = self.replace_digits(x)
        
        # encode x
        # prepare x to be passed to keras sequence padding unction
#        x = [x]
        x = self.pad([x], maxlen=self.maxlen, padding='pre', dtype=str)[0]
        x = self.encode_x_utterance(x)
        
        #reshape X
        x = np.array(x)
        x = x.reshape(1, x.shape[0], x.shape[1])
        
        return x
    
    def convert_to_iob_tags(self, y):
        _2d = self.__decode_classifier_output(y)
        decoded = _2d[0]
        decoded = self.y_encoder.decode(decoded)
        return decoded

    def __decode_classifier_output(self, arr3d):
        output_2d = np.zeros(shape=(arr3d.shape[0], arr3d.shape[1]), dtype=int)
        for i in range(arr3d.shape[0]):
            output_2d[i] = [j.argmax() for j in arr3d[i]]
            
        return output_2d
    
    def save(self, path):
        with open(path, 'wb') as pre_file:
            pickle.dump(self, pre_file, protocol=pickle.HIGHEST_PROTOCOL)
         
    @staticmethod
    def load(path):
        preprocessor = None
        with open(path, 'rb') as f:
            preprocessor = pickle.load(f)
        return preprocessor
    
    
if __name__ == '__main__':
    pre = Preprocessing(embeddings_file = '/home/mostafa/Mostafa/running_projects/lstm_intent_slot_filling/resources/word_embedding_based/unit_test_data/test_embeddings.txt')
    print(pre.embeddings_index)
    encoded_utterance = pre.encode_X_utterance(['12'])
    print(encoded_utterance)
    
    # experiments
#    pre = Preprocessing(embeddings_file = '/home/mostafa/Mostafa/running_projects/lstm_intent_slot_filling/resources/word_embedding_based/glove.840B.300d.txt')
#    dic = pre.embeddings_index
    
    