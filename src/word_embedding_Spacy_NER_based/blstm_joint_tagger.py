#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 19 16:07:26 2017

@author: mostafa
"""
from keras.models import Sequential, load_model
from keras.layers import LSTM
from keras.layers.embeddings import Embedding
from keras.layers import Dense, TimeDistributed, Bidirectional
import tensorflow as tf

class BLSTMJointTagger:
    
    def __init__(self, num_bidirectional_layer=1):
        self.graph = tf.get_default_graph()
        self.num_bidirectional_layer = num_bidirectional_layer

    #useful article: https://machinelearningmastery.com/develop-bidirectional-lstm-sequence-classification-python-keras/ 
    def train(self, x, y, optimizer = 'Adam', loss = 'categorical_crossentropy',
              epochs = 10, batch_size = 10, validation_split=0.2,
              vocab_size = None):
        
        nClass = y.shape[2]
    
        # define the network architecture
        self.classifier = Sequential()
        
        # add first lstm layer
        self.classifier.add(Bidirectional(LSTM(150, return_sequences=True), input_shape = (x.shape[1], x.shape[2]), merge_mode='concat'))
        
        # add LSTM layers
        for i in range(self.num_bidirectional_layer):
            self.classifier.add(Bidirectional(LSTM(150, return_sequences=True), merge_mode='concat'))
        
        # add Dense output layer inside time distributed
        self.classifier.add(TimeDistributed(Dense(units = nClass, activation='softmax')))
        # compile the network
        self.classifier.compile(loss = loss, optimizer = optimizer, metrics=['categorical_accuracy'])
        # model summary
        print(self.classifier.summary())
        # train the model
        self.classifier.fit(x, y, validation_split = validation_split, epochs = epochs, batch_size = batch_size)

    def test(self, x, y):
        metrics_values = self.classifier.evaluate(x=x, y=y)
        metrics = self.classifier.metrics_names
        return {metrics[0]:metrics_values[0], metrics[1]:metrics_values[1]}
    
    def predict(self, x):
        y = None
        with self.graph.as_default():
            y = self.classifier.predict(x)
        return y
        
    def save(self, path):
        self.classifier.save(path)
        
    @staticmethod
    def load(path):
        tagger = BLSTMJointTagger()
        tagger.classifier = load_model(path)
        return tagger