#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 27 10:08:09 2017

@author: wahdan
"""

# imports
from preprocessing import Preprocessing
from blstm_joint_tagger import BLSTMJointTagger

# parameters
testing_file = '../../resources/word_embedding_Spacy_NER_based/test/atis.test.w-intent.iob'
model_file = '../../resources/word_embedding_Spacy_NER_based/model/intent_slot_filling.model'
preprocessing_file = '../../resources/word_embedding_Spacy_NER_based/model/intent_slot_filling_preprocessing.pickle'

# load model
print('loading model ....')
tagger = BLSTMJointTagger.load(model_file)
preprocessor = Preprocessing.load(preprocessing_file)

# preprocessing
print('start testing ....')
X, Y = preprocessor.preprocess_test_data(testing_file)

# test data
evaluation = tagger.test(X, Y)
for key, value in evaluation.items():
    print('%s: %.4f' % (key, value))
