#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 27 11:05:18 2017

@author: wahdan
"""

# imports
from preprocessing import Preprocessing
from blstm_joint_tagger import BLSTMJointTagger

# parameters
testing_file = '../../resources/word_embedding_Spacy_NER_based/test/atis.test.w-intent.iob'
model_file = '../../resources/word_embedding_Spacy_NER_based/model/intent_slot_filling.model'
preprocessing_file = '../../resources/word_embedding_Spacy_NER_based/model/intent_slot_filling_preprocessing.pickle'

# load model
print('loading model ....')
tagger = BLSTMJointTagger.load(model_file)
preprocessor = Preprocessing.load(preprocessing_file)

# predict
print('Enter, utterance')
while True:
    utterance = input()
    if utterance == 'exit':
        break
    input_vector = preprocessor.preprocess_x(utterance)
    output_vector = tagger.predict(input_vector)
    tag_sequence = preprocessor.convert_to_iob_tags(output_vector)
    print(tag_sequence)
