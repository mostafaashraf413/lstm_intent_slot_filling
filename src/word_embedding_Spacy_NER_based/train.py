#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 25 09:50:58 2017

@author: wahdan
"""

from preprocessing import Preprocessing
from blstm_joint_tagger import BLSTMJointTagger

# parameters
training_file = '../../resources/word_embedding_Spacy_NER_based/train/atis.train.w-intent.iob'
embeddings_file = '../../resources/crawl-300d-2M.vec'
save_model_to = '../../resources/word_embedding_Spacy_NER_based/model/intent_slot_filling.model'
save_preprocessing_to = '../../resources/word_embedding_Spacy_NER_based/model/intent_slot_filling_preprocessing.pickle'

# preprocessing
print('start preprocessing ....')
preprocessor = Preprocessing(embeddings_file = embeddings_file)
X, Y= preprocessor.preprocess_training_data(training_file)

# train the classifier
print('start training ....')
tagger = BLSTMJointTagger()
tagger.train(X, Y)

# save model weights and preprocessing (encoders)
print('saving model  ....')
tagger.save(save_model_to)
preprocessor.save(save_preprocessing_to)
